const mongoose = require('mongoose')
const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    minlength: 2,
    unique: true
  },
  type: {
    type: String,
    enum: ['วิชาการ', 'เตรียมความพร้อม'],
    required: true
  },
  date: {
    type: Date,
    required: true
  },
  time: {
    type: String,
    required: true
  },
  room: {
    type: String,
    required: true,
    minlength: 1,
    unique: true
  },
  hours: {
    type: Number,
    required: true,
    min: 1,
    max: 24
  },
})

module.exports = mongoose.model('User', userSchema)