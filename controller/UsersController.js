const User = require('../models/User')
const userController = {
  // userList: [
  //   {
  //     id: 1,
  //     name: "UX/UI Design",
  //     type: "วิชาการ",
  //     date: "2018-12-19 20:15",
  //     time: "13.00-16.00",
  //     room: "ห้องประชุม IF-120",
  //     hours: 3
  //   },
  //   {
  //     id: 2,
  //     name: 'G-able',
  //     type: 'เตรียมความพร้อม',
  //     date: '2018-12-19 20:15',
  //     time: '13.00-16.00',
  //     room: 'ห้องประชุม IF-4M210',
  //     hours: 3
  //   }
  // ],
  lastId: 3,
  addUser (req, res, next) {
    const payload = req.body

    const user = new User(payload)
    
    User.create(payload).then(function (users) {
      users.id = this.lastId++
      res.json(users)
    }).catch(function (err) {
      res.status(500).send(err)
    })
  },
  // async addUser(req, res, next) {
  //   const payload = req.body
  //   const user = new User(payload)
  //   try {
  //     const newuser = await user.save()
  //     res.json(newuser)
  //   } catch (err) {
  //     res.status(500).send(err)
  //   }
  // },
  async updateUser(req, res, next) {
    const payload = req.body
    // res.json(usersController.updateUser(payload))
    // const index = 
    try {
      const user = await User.updateOne({ _id: payload._id }, payload)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteUser(req, res, next) {
    const { id } = req.params
    // res.json(usersController.deleteUser(id))
    try {
      const user = await User.deleteOne({ _id: id })
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUsers(req, res, next) {
    try {
      const users = await User.find({})
      res.json(users)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUser(req, res, next) {
    const { id } = req.params
    try {
      const user = await User.findById(id)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)

    }
  }
}

module.exports = userController
