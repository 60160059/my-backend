"use strict";

var Hour = require('../models/Hour');

var hourController = {
  // hourList: [
  //   {
  //     id: 1,
  //     idPerson: '59160020',
  //     name: 'นายประจักร ขมักขมิ่น',
  //     branch: 'วิทยาการคอมพิวเตอร์',
  //     ready: '21',
  //     academic: '8'
  //   },
  //   {
  //     id: 2,
  //     idPerson: '59160123',
  //     name: 'นายสมชาย แมนมาก',
  //     branch: 'วิทยาการคอมพิวเตอร์',
  //     date: '16 กุมภาพันธ์  พ.ศ.2562',
  //     ready: '30',
  //     academic: '15'
  //   }
  // ],
  lastId: 3,
  addHour: function addHour(req, res, next) {
    var payload = req.body;
    Hour.create(payload).then(function (hours) {
      res.json(hours);
    })["catch"](function (err) {
      res.status(500).send(err);
    });
  },
  // async addUser(req, res, next) {
  //   const payload = req.body
  //   const user = new User(payload)
  //   try {
  //     const newuser = await user.save()
  //     res.json(newuser)
  //   } catch (err) {
  //     res.status(500).send(err)
  //   }
  // },
  updateHour: function updateHour(req, res, next) {
    var payload, hour;
    return regeneratorRuntime.async(function updateHour$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            payload = req.body; // res.json(usersController.updateUser(payload))

            _context.prev = 1;
            _context.next = 4;
            return regeneratorRuntime.awrap(Hour.updateOne({
              _id: payload._id
            }, payload));

          case 4:
            hour = _context.sent;
            res.json(hour);
            _context.next = 11;
            break;

          case 8:
            _context.prev = 8;
            _context.t0 = _context["catch"](1);
            res.status(500).send(_context.t0);

          case 11:
          case "end":
            return _context.stop();
        }
      }
    }, null, null, [[1, 8]]);
  },
  deleteHour: function deleteHour(req, res, next) {
    var id, hour;
    return regeneratorRuntime.async(function deleteHour$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            id = req.params.id; // res.json(usersController.deleteUser(id))

            _context2.prev = 1;
            _context2.next = 4;
            return regeneratorRuntime.awrap(Hour.deleteOne({
              _id: id
            }));

          case 4:
            hour = _context2.sent;
            res.json(hour);
            _context2.next = 11;
            break;

          case 8:
            _context2.prev = 8;
            _context2.t0 = _context2["catch"](1);
            res.status(500).send(_context2.t0);

          case 11:
          case "end":
            return _context2.stop();
        }
      }
    }, null, null, [[1, 8]]);
  },
  getHours: function getHours(req, res, next) {
    var hours;
    return regeneratorRuntime.async(function getHours$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.prev = 0;
            _context3.next = 3;
            return regeneratorRuntime.awrap(Hour.find({}));

          case 3:
            hours = _context3.sent;
            res.json(hours);
            _context3.next = 10;
            break;

          case 7:
            _context3.prev = 7;
            _context3.t0 = _context3["catch"](0);
            res.status(500).send(_context3.t0);

          case 10:
          case "end":
            return _context3.stop();
        }
      }
    }, null, null, [[0, 7]]);
  },
  getHour: function getHour(req, res, next) {
    var id, hour;
    return regeneratorRuntime.async(function getHour$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            id = req.params.id;
            _context4.prev = 1;
            _context4.next = 4;
            return regeneratorRuntime.awrap(Hour.findById(id));

          case 4:
            hour = _context4.sent;
            res.json(hour);
            _context4.next = 11;
            break;

          case 8:
            _context4.prev = 8;
            _context4.t0 = _context4["catch"](1);
            res.status(500).send(_context4.t0);

          case 11:
          case "end":
            return _context4.stop();
        }
      }
    }, null, null, [[1, 8]]);
  }
};
module.exports = hourController;