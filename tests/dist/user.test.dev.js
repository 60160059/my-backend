"use strict";

var dbHandler = require('./db-handler');

var User = require('../models/User');

beforeAll(function _callee() {
  return regeneratorRuntime.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return regeneratorRuntime.awrap(dbHandler.connect());

        case 2:
        case "end":
          return _context.stop();
      }
    }
  });
});
afterEach(function _callee2() {
  return regeneratorRuntime.async(function _callee2$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return regeneratorRuntime.awrap(dbHandler.clearDatabase());

        case 2:
        case "end":
          return _context2.stop();
      }
    }
  });
});
afterAll(function _callee3() {
  return regeneratorRuntime.async(function _callee3$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.next = 2;
          return regeneratorRuntime.awrap(dbHandler.closeDatabase());

        case 2:
        case "end":
          return _context3.stop();
      }
    }
  });
});
var userComplete1 = {
  name: 'Kob',
  type: 'วิชาการ'
};
var userComplete2 = {
  name: 'Kob',
  type: 'เตรียมความพร้อม'
};
var userErrorNameEmpty = {
  name: '',
  gender: 'M'
};
var userErrorName2Alphabets = {
  name: 'Ko',
  gender: 'M'
};
var userErrorGenderInvalid = {
  name: 'Kob',
  gender: 'A'
};
describe('User', function () {
  it('สามารถเพิ่ม user ได้ วิชาการ', function _callee4() {
    var error, user;
    return regeneratorRuntime.async(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            error = null;
            _context4.prev = 1;
            user = new User(userComplete1);
            _context4.next = 5;
            return regeneratorRuntime.awrap(user.save());

          case 5:
            _context4.next = 10;
            break;

          case 7:
            _context4.prev = 7;
            _context4.t0 = _context4["catch"](1);
            error = _context4.t0;

          case 10:
            expect(error).toBeNull();

          case 11:
          case "end":
            return _context4.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('สามารถเพิ่ม user ได้ เตรียมความพร้อม', function _callee5() {
    var error, user;
    return regeneratorRuntime.async(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            error = null;
            _context5.prev = 1;
            user = new User(userComplete2);
            _context5.next = 5;
            return regeneratorRuntime.awrap(user.save());

          case 5:
            _context5.next = 10;
            break;

          case 7:
            _context5.prev = 7;
            _context5.t0 = _context5["catch"](1);
            error = _context5.t0;

          case 10:
            expect(error).toBeNull();

          case 11:
          case "end":
            return _context5.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่ม user ได้ เพราะ name เป็น ช่องว่าง', function _callee6() {
    var error, user;
    return regeneratorRuntime.async(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            error = null;
            _context6.prev = 1;
            user = new User(userErrorNameEmpty);
            _context6.next = 5;
            return regeneratorRuntime.awrap(user.validate());

          case 5:
            _context6.next = 10;
            break;

          case 7:
            _context6.prev = 7;
            _context6.t0 = _context6["catch"](1);
            error = _context6.t0;

          case 10:
            expect(error).not.toBeNull();

          case 11:
          case "end":
            return _context6.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่ม user ได้ เพราะ name เป็น 2 ตัว', function _callee7() {
    var error, user;
    return regeneratorRuntime.async(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            error = null;
            _context7.prev = 1;
            user = new User(userErrorName2Alphabets);
            _context7.next = 5;
            return regeneratorRuntime.awrap(user.save());

          case 5:
            _context7.next = 10;
            break;

          case 7:
            _context7.prev = 7;
            _context7.t0 = _context7["catch"](1);
            error = _context7.t0;

          case 10:
            expect(error).not.toBeNull();

          case 11:
          case "end":
            return _context7.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่ม user ได้ gender ไม่ถูกต้อง A', function _callee8() {
    var error, user;
    return regeneratorRuntime.async(function _callee8$(_context8) {
      while (1) {
        switch (_context8.prev = _context8.next) {
          case 0:
            error = null;
            _context8.prev = 1;
            user = new User(userErrorGenderInvalid);
            _context8.next = 5;
            return regeneratorRuntime.awrap(user.save());

          case 5:
            _context8.next = 10;
            break;

          case 7:
            _context8.prev = 7;
            _context8.t0 = _context8["catch"](1);
            error = _context8.t0;

          case 10:
            expect(error).not.toBeNull();

          case 11:
          case "end":
            return _context8.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่ม user ชื่อซ้ำกันได้', function _callee9() {
    var error, user1, user2;
    return regeneratorRuntime.async(function _callee9$(_context9) {
      while (1) {
        switch (_context9.prev = _context9.next) {
          case 0:
            error = null;
            _context9.prev = 1;
            user1 = new User(userComplete1);
            _context9.next = 5;
            return regeneratorRuntime.awrap(user1.save());

          case 5:
            user2 = new User(userComplete2);
            _context9.next = 8;
            return regeneratorRuntime.awrap(user2.save());

          case 8:
            _context9.next = 13;
            break;

          case 10:
            _context9.prev = 10;
            _context9.t0 = _context9["catch"](1);
            error = _context9.t0;

          case 13:
            expect(error).not.toBeNull();

          case 14:
          case "end":
            return _context9.stop();
        }
      }
    }, null, null, [[1, 10]]);
  });
});